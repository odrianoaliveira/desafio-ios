//
//  RepoCell.swift
//  Desafio iOS
//
//  Created by Adriano on 10/01/17.
//  Copyright © 2017 Adriano. All rights reserved.
//

import UIKit
import Haneke

class RepoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var repoName: UILabel!
    @IBOutlet weak var repoDescr: UILabel!
    
    @IBOutlet weak var numForks: UILabel!
    @IBOutlet weak var numStars: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var fullName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        repoName?.text = nil
        repoDescr?.text = nil
        numForks?.text = "0 forks"
        numStars?.text = "0 stars"
        userImage?.image = nil
        userName?.text = nil
        fullName?.text = nil
        userImage.hnk_cancelSetImage()
    }
    
}
