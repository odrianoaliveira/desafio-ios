//
//  PullRequestsTableViewController.swift
//  Desafio iOS
//
//  Created by Adriano on 11/01/17.
//  Copyright © 2017 Adriano. All rights reserved.
//

import UIKit
import SwiftyJSON

class PullRequestsTableViewController: UITableViewController {
    
    var pullRequestData: PullRequestSegueVO?
    var items = JSON.null
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = pullRequestData?.repositoryName
        
        GitHubAPIManager.sharedInstance.getPullRequests(login: pullRequestData!.login, repositoryName: pullRequestData!.repositoryName, completion:  {
            (json, error) in
            
            if (json != JSON.null) {
                self.items = json
                self.tableView.reloadData()
            } else {
                log.error("error in git hub request\(error)");
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.items.count
        return count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestCell", for: indexPath) as! PullRequestTableViewCell
        
        let item = self.items[indexPath.row]
        
        cell.titleLabel.text = item["title"].string
        cell.descriptionLabel.text = item["body"].string
        cell.userImage.hnk_setImage(from: item["user"]["avatar_url"].url)
        cell.username.text = item["user"]["login"].string

        return cell
    }
}
