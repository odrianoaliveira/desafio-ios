//
//  PullRequestTableViewCell.swift
//  Desafio iOS
//
//  Created by Adriano on 11/01/17.
//  Copyright © 2017 Adriano. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var username: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        titleLabel?.text = nil
        descriptionLabel?.text = nil
        userImage.hnk_cancelSetImage()
        username?.text = nil
    }
}
