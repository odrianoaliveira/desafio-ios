//
//  RepoTableViewController.swift
//  Desafio iOS FTW
//
//  Created by Adriano on 10/01/17.
//  Copyright © 2017 Adriano. All rights reserved.
//

import UIKit
import SwiftyJSON
import Haneke

class RepoTableViewController: UITableViewController {
    
    private var page = 1
    private var items = JSON.null
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "GitHub Java Repositories"
        
        GitHubAPIManager.sharedInstance.getRepositoriesPage(page: page, completion: { (json, error) in
            if (json != JSON.null) {
                self.items = json["items"]
                self.tableView.reloadData()
            } else {
                log.error("error in git hub request\(error)");
            }
        })
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if items == JSON.null {
            return
        }
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        var reLoad = false
        
        if offsetY < -100 && self.page > 1 {
            self.page -= 1;
            reLoad = true
            items = JSON.null
        }
        
        if offsetY > ((contentHeight - scrollView.frame.size.height) + 50) {
            self.page += 1
            reLoad = true
            items = JSON.null
        }
        
        if reLoad {
            GitHubAPIManager.sharedInstance.getRepositoriesPage(page: self.page, completion: { (json, error) in
                if (json != JSON.null) {
                    self.items = json["items"]
                    scrollView.setContentOffset(CGPoint(x:0, y:44), animated: true)
                    self.tableView.reloadData()
                } else {
                    log.error("error in git hub request\(error)");
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = items.count
        return count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepoCell", for: indexPath) as! RepoTableViewCell
        
        let item = items[indexPath.row]
        
        if item != JSON.null {
            log.debug(item)
            
            cell.repoName.text = item["name"].string
            cell.repoDescr.text = item["description"].string
            if let forks = item["forks"].number {
                cell.numForks.text = "\(forks) forks"
            }
            if let stars = item["stargazers_count"].number {
                cell.numStars.text = "\(stars) stars"
            }
            cell.userName.text = item["owner"]["login"].string
            cell.fullName.text = item["full_name"].string
            
            if let url = item["owner"]["avatar_url"].url {
                cell.userImage.hnk_setImage(from: url)
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        performSegue(withIdentifier: "ShowPullRequests", sender: PullRequestSegueVO(login: item["owner"]["login"].string!, repositoryName: item["name"].string!))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowPullRequests" {
            if let pullRequestSegueVO = sender as? PullRequestSegueVO {
                log.debug(pullRequestSegueVO.login)
                log.debug(pullRequestSegueVO.repositoryName)

                if let vc = segue.destination as? PullRequestsTableViewController {
                    vc.pullRequestData = pullRequestSegueVO
                }
            }
        }
    }
}
