//
//  PullRequestSegueVO.swift
//  Desafio iOS
//
//  Created by Adriano on 11/01/17.
//  Copyright © 2017 Adriano. All rights reserved.
//

import Foundation

struct PullRequestSegueVO {
    var login: String
    var repositoryName: String
    
    init(login: String, repositoryName: String) {
        self.login = login
        self.repositoryName = repositoryName
    }
}

