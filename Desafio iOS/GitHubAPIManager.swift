//
//  GitHubAPIManager.swift
//  Desafio iOS
//
//  Created by Adriano on 10/01/17.
//  Copyright © 2017 Adriano. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum GitHubAPIError : Error {
    case RuntimeError(String)
}

typealias CallBack = (_: JSON, _: Error?) -> Void

class GitHubAPIManager {
    static let sharedInstance = GitHubAPIManager()
    let baseURL = "https://api.github.com"
    
    func getRepositoriesPage(page: Int, completion: @escaping CallBack ) {
        
        let url = "\(baseURL)/search/repositories?q=language:Java&sort=stars&page=\(page)"
        
        Alamofire.request(url)
            .responseData { response in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        completion(JSON(data: data), nil)
                    } else {
                        completion(JSON.null, GitHubAPIError.RuntimeError("Unable to get json data from url=\(url)"))
                    }
                case .failure(let error):
                    completion(JSON.null, error)
                }
        }
    }
    
    func getPullRequests(login: String, repositoryName: String, completion: @escaping CallBack) {
        
        let url = "\(baseURL)/repos/\(login)/\(repositoryName)/pulls"
        Alamofire.request(url)
            .responseData { response in
                switch response.result {
                case .success:
                    if let data = response.result.value {
                        completion(JSON(data: data), nil)
                    } else {
                        completion(JSON.null, GitHubAPIError.RuntimeError("Unable to get json data from url=\(url)"))
                    }
                case .failure(let error):
                    completion(JSON.null, error)
                }
        }
    }
}


